// ADD_USER
export const addUser = (
    { 
        name = '', 
        email = '', 
        zipcode = '', 
        phone = '' 
    } = {}
) => ({
    type: 'ADD_USER',
    users: {
        id,
        name,
        email,
        zipcode,
        phone
    }
});