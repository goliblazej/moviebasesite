//SET_TEXT_FLITER
export const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text
});