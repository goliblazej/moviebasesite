import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import NotFoundPage from '../components/NotFoundPage';
import Main from '../components/Main';

const AppRouter = () => (
    <BrowserRouter>
            <Switch>
                <Route path="/" component={ Main } exact={true}/>
                <Route component={ NotFoundPage } />
            </Switch>
    </BrowserRouter>
); 

export default AppRouter;
