// Get visible users
export default (users, { text, sortBy }) => {
    return users.filter((user) => {
        const textMatch = user.name.toLowerCase().includes(text.toLowerCase());

        return  textMatch;
    }).sort((a, b) => {
        if(sortBy === 'name') {
            return a.name < b.name ? 1 : -1;
        }
    });
};