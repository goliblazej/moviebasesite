import React from 'react';
import MovieForm from './MovieForm';
import SearchBar from './SearchBar';

var sectionStyle = {
    main: {
        width: "100%",
        height: "100%",
        paddingTop: "50px"
    }
};

const Main = () => (
    <div style={ sectionStyle.main }>
        <SearchBar />
        <MovieForm />
        <div className="page-bg">
        </div>
    </div>
);

export default Main;