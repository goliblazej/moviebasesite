import React from 'react';
import MovieSummary from './MovieSummary';
import MovieDetailGrid from './MovieDetailGrid';

var sectionStyle = {
    paddingLeft: "25px",
    paddingRight: "25px",
    position: "relative",
};

const MovieFormText = () => (
    <div style = {sectionStyle}>
        <MovieSummary /> 
        <MovieDetailGrid />      
    </div>
);

export default MovieFormText;