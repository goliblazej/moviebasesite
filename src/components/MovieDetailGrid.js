import React from 'react';
import Grid from '@material-ui/core/Grid';
import MovieDetail from './MovieDetail';

var sectionStyle = {
    main: {
        position: "absolute",
        bottom: "0",
        paddingBottom: "10px"
    },
    noBottomPadding: { 
        paddingBottom: "0px"
    },
    noTopPadding: { 
        paddingTop: "0px"
    }
};

const MovieDetailGrid = () => (
    <Grid container spacing={24} style={ sectionStyle.main }>

        <Grid item xs={6} style={ sectionStyle.noBottomPadding }>
            <MovieDetail InfoKey="Original Release:" InfoBody="2009-06-02"/>
        </Grid>

        <Grid item xs={6} style={ sectionStyle.noBottomPadding }>
            <MovieDetail InfoKey="Running Time:" InfoBody="100 mins"/>
        </Grid>

        <Grid item xs={6} style={ sectionStyle.noTopPadding }>
            <MovieDetail InfoKey="Box Office:" InfoBody="$459,270,619"/>
        </Grid>

        <Grid item xs={6} style={ sectionStyle.noTopPadding }>
            <MovieDetail InfoKey="Vote Average:" InfoBody="7.2 / 10"/>
        </Grid>

    </Grid>
);

export default MovieDetailGrid;