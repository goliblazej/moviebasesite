import React from 'react';
import { Typography } from '../../node_modules/@material-ui/core';


var sectionStyle = {
    infoKey: {
        color: "white",
        fontSize: "20px",
        fontWeight: "300"
    },
    infoBody: {
        color: "#00FC87",
        fontSize: "24px"
    },
};

const MovieDetail = (props) => (
    <div>
        <Typography variant="body2" style = {sectionStyle.infoKey}>
            {props.InfoKey}
        </Typography>
        <Typography variant="body1" style = {sectionStyle.infoBody} gutterBottom>
            {props.InfoBody}
        </Typography>
    </div>
);

export default MovieDetail;