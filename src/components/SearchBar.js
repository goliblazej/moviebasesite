import React from 'react';
import Select from 'react-select';
import styled from 'react-emotion';

var sectionStyle = {
    width: "40%",
    marginBottom: "15px",
    marginRight: "auto", 
    marginLeft: "auto"
};

const customStyles = {
    option: (styles, { isFocused, isSelected, isDisabled }) => {
        return {
            ...styles, 
            color: "white", 
            backgroundColor: 
                            isDisabled ?  null : 
                            isSelected ? "rgb(0, 252, 135)" : 
                            isFocused ? "rgb(0, 252, 135)" : null,
            
        }
    },
    control: (styles, { isFocused }) => {
      return { 
          ...styles, 
          //backgroundColor: state.isFocused ? 'rgba(0, 0, 0, 0.75)' : 'rgba(0, 0, 0, 0.45)',
          backgroundColor: 'rgba(0, 0, 0, 0.65)',
          border: isFocused? 'none' : 'none'
      }
      
    },
    singleValue: (base) => {
      //const opacity = 0.85;
      //const transition = 'opacity 300ms';
  
      return { ...base, color: "white"};
    },
    placeholder: (base) => ({
        ...base,
        color: "grey"
     }),
     input: (base) => ({
         ...base,
         color: "white"
     }),
     container: (base) => {
        return { ...base, border: "none"};
     },
     menuList: (base) => {
        return { ...base};
     },
     menu: (base) => {
        return { ...base, backgroundColor: 'rgba(0, 0, 0, 0.90)'};
     }
  }


const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
];


const SearchBar = () => (
    <div style={ sectionStyle }>
        <Select 
            options={options} 
            styles={customStyles}
            placeholder="Search movie title" />
    </div>
)

export default SearchBar;

