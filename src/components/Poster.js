import React from 'react';

var sectionStyle = {
    width: "100%",
    height: "auto",
    maxHeight: "480px",
    maxWidth: "320px",
    //margin: 'auto 0 auto 0',
    display: 'block'
};

const Poster = () => (
    <img 
        style={ sectionStyle } 
        src="https://image.tmdb.org/t/p/w500/uluhlXubGu1VxU63X9VHCLWDAYP.jpg"
    />
);

export default Poster;