import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Poster from './Poster';
import MovieFormText from './MovieFormText';

var sectionStyle = {
    gridToHalfs: {
        marginRight: "auto", 
        marginLeft: "auto", 
        width: "60%",
        // height: "500px",
        minWidth: "780px",
        maxWidth: "850px",
        // overflow: "hidden",
        // display:
        display: "flex",
        backgroundColor: 'rgba(0, 0, 0, 0.75)'
    },
    poster: {
        // maxHeight: "500px"
        width: "280px"
    },
    gridTemp: {
        height: "100%"
    },
    movieFormText: {
        flexGrow: "1"
    }
};

const MovieForm = () => (
    <div>
    <Paper style={ sectionStyle.gridToHalfs } elevation={1}>
        <Poster style={ sectionStyle.poster }/>
        <MovieFormText style={ sectionStyle.movieFormText }/>
        {/* <Grid container spacing={0}>
            <Grid style = { sectionStyle.gridTemp} item xs={6}>
                <Poster style={ sectionStyle.poster }/>
            </Grid>
            <Grid item xs={6}>
                <MovieFormText />
            </Grid>
        </Grid> */}
    </Paper>
    </div>
);


export default MovieForm;