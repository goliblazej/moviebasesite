//
//  DEFAULT STATE GENERATION 
//      (to be removed)

let id = 0;
function createData(name, dob, email, phone, address) {
  id += 1;
  return { id, name, dob, email, phone, address };
}

const data = [
    createData(1, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(2, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(3, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(4, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(5, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(6, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(7, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(8, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(9, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(10, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(11, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(12, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(13, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(14, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street'),
    createData(15, 'Mary Matthews', '10/11/1990', 'mary.matthews@test.com', '111-222-5555', '7456 Michigan Avenue'),
    createData(16, 'Harry Hall', '10/10/1985', 'harry.hall@example.com', '888-999-1111', '4123 Say No More Street')
];


// USERS REDUCER

const usersDefaultState = {
    data : data
};

export default (state = usersDefaultState, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return {
                ...state,
                text: action.text
            };
        default:
            return state;
    }
};




